<?php Func::login_security(); ?>
<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <title>VidSpin : An awesome video spinner</title>
    <meta name="keyword" content="Alternative to HiTechVideoPro">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo SITE_URL ?>/assets/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo SITE_URL ?>/assets/css/tagmanager.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets/css/style.css" />
    <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets/css/animation.css" data-skrollr-stylesheet />
    <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets/css/responsive.css" />
    <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets/css/jquery.toastmessage.css" />

    <script type="text/javascript" src="//use.typekit.net/npr5xhc.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript" src="<?php echo SITE_URL ?>/assets/js/modernizr.min.js"></script>
    

  </head>
  <body <?php Func::body_class() ?>>
    <?php $current_page = isset($_GET['page']) ? $_GET['page'] : FRONT_PAGE; ?>
    <?php if ($current_page != 'registration'): ?>
    <nav id="access" class="clearfix">
      <div class="container">
        <ul class="left">
          <li><a href="#" class="btn">Home</a></li>
          <li><a href="#features" class="btn slide-to">Features</a></li>
          <li><a href="#pricing" class="btn slide-to">Price</a></li>
          <li><a href="blog/" class="btn">Blog</a></li>
          <li><a href="?page=support" class="btn">Contact Us</a></li>       
        </ul>
        <ul class="right">
           <?php if (isset($_SESSION['email'])): ?>
           <li><a href="?page=dashboard&status=logout" class="btn slide-to">Signout</a></li>
           <?php else: ?>
              <?php if (isset($_SESSION['email'])): ?>
             <li><a href="#?page=dashboard&status=logout" class="btn slide-to">Signout</a></li>
         <?php else: ?>
            <li><a href="#" class="btn" data-toggle="modal" data-target="#user_login">Sign-In</a></li>
            <li><a href="#" class="btn" data-toggle="modal" data-target="#pricing">Register</a></li>
          <?php endif ?>
      <?php endif ?>
       
        </ul>
      </div>
    </nav>
    <nav id="access-mobile" class="responsive">
      <a href="#" id="menu-navigator">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <ul>
        <li><a href="#" class="btn">Home</a></li>
        <li><a href="#features" class="btn slide-to">Features</a></li>
        <li><a href="#pricing" class="btn slide-to">Price</a></li>
        <li><a href="#" class="btn">Blog</a></li>
        <li><a href="mailto:team@videospinner.com" class="btn">Contact Us</a></li>   
        <li class="separator"></li>
        <li><a href="#" class="btn" data-toggle="modal" data-target="#user_login">Sign-In</a></li>
        <li><a href="#" class="btn" data-toggle="modal" data-target="#pricing">Register</a></li>
      </ul>
    </nav>
    
      <?php endif ?>
      <div class="modal fade" id="form_avatar" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">     
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h2>My Avatar</h2>
          <hr>
          <div id="signup-wrapper">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
              <div id="browse-avatar">
                <div class="form-div">
                  <div id="avatar-signup">
                    <img id="profile-avatar" src="<?php echo SITE_URL ?>/assets/images/sample-avatar.png" alt="..." class="img-thumbnail">
                    <a href="" id="upload_link" style="text-decoration: none;"><p>Browse</p></a>
                  <div id="error-upload"></div>
                  </div>
                          <input type="file" class="form-control" id="avatar_prof" name="avatar_prof" style="display: none;">
                </div>
                <div id="btn-submit-container">
                  <div class="form-div-btn">
                    <input type="submit" id="btn-submit-avatar" name="btn-submit-avatar" class="btn btn-primary form-control" value="Change">
                  </div>
                </div>
              </div> <!-- ==== // id: browse-avatar-->
            </form> 
          </div> <!-- ====== // id: signup-wrapper ===== -->  
        </div>      
      </div>
    </div>
</div>

     