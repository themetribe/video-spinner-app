<div id="sidebar">
	 <?php if ($_SESSION['avatar'] != ""): ?>
	 	<img id="avatar" src="<?php echo $_SESSION['avatar']; ?>" alt="..." class="img-thumbnail">
	 	    <a href="#" class="btn" data-toggle="modal" data-target="#form_avatar" style="position: relative;top: -15px;">change</a>	
	 <?php else: ?>
	   <img id="avatar" src="<?php echo SITE_URL ?>/assets/images/sample-avatar.png" alt="..." class="img-thumbnail">
	    <a href="#" class="btn" data-toggle="modal" data-target="#form_avatar" style="position: relative;top: -15px;">change</a>	
	 <?php endif ?>
	 	<p>Welcome <strong><?php echo $_SESSION['name'];?></strong>!  <a href="?page=dashboard&status=logout">Logout</a></p>	
	<p><a class="btn btn-default" href="?page=user_info">Settings</a></p>
</div>

