    <div id="footer">
        <div class="container">
            Videospinner &copy 2013 v1.0
        </div>
    </div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo SITE_URL ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/ajax.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/main.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/tagmanager.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/scripts.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/avatar.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/modal.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/tooltip.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/popover.js"></script>
    <script src="<?php echo SITE_URL ?>/assets/js/jquery.toastmessage.js"></script>
	<?php
	//execute_footer_hook();
	?>
    <script src="<?php echo SITE_URL ?>/assets/js/skrollr.stylesheets.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL ?>/assets/js/skrollr.js"></script>
    <script>var s = skrollr.init();</script>
  </body>
</html>