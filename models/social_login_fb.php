<?php 
	
	include("models/cls_facebook.php");
	class Fb_login extends DB{
		function __construct(){
		parent::__construct(); 
		$this->table = "user";
		$facebook = new Facebook(array(
		  'appId'  => appid,
		  'secret' => appsecret,
		));
	}
	
	// Get User ID
	$user_fb = $facebook->getUser();

	function user_login(){
		global $facebook;
		//$statusUrl = $facebook->getLoginStatusUrl();
		  return $facebook->getLoginUrl( array(
	                'scope'         => 'email' 
	            )
	  		);
		}
	function get_user_info(){
	    global $facebook;
	    global $user_fb;
	    if($user_fb){
			try {
			    // Proceed knowing you have a logged in user who's authenticated.
			   $user_profile = $facebook->api('/me');
			   return print_r($user_profile);
			  } catch (FacebookApiException $e) {
			    error_log($e);
			    $user = null;
		  }  		    	
	    }    
	}

	
	function user_logout(){
		$param = array('next'=>'http://localhost/fb/example/logout.php');
  		$logoutUrl = $facebook->getLogoutUrl( $param);
	}
	function destroy_fb_session(){
		global $facebook;
		$facebook->destroySession();
	}

	function insert_user_info(){
		 global $facebook;
	     global $user_fb;

	     if ($user_fb) {
	     	$avatar = "https://graph.facebook.com/{$user}";
	     	$user = $this->get_user_info();
	     	$_data = array("first_name"=>$user['first_name'], "last_name"=>$user['last_name'], "email"=>$user['email'],
	     					"avatar"=>$avatar, "verification_code"=>1);
	     	$check_email =  $user->select("email", array("email"=>$$user['email']),true, "user" );
			if ($check_email==false){
				session_start();
				$data = $user->save($_data);
				$_SESSION['fname'] = $user['first_name'];
				$_SESSION['email'] = $user['email'];
				header("Location:?page=dashboard");
				exit();
			}
	     }
	}

}	
 ?>