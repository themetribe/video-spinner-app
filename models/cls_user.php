<?php
class User extends DB{
	function __construct(){
		parent::__construct(); 
		$this->table = "user";
		$this->verification_code=md5(uniqid(rand())); 
		
	}
	function randomAlphaNum($length){ 
	    $rangeMin = pow(36, $length-1); //smallest number to give length digits in base 36 
	    $rangeMax = pow(36, $length)-1; //largest number to give length digits in base 36 
	    $base10Rand = mt_rand($rangeMin, $rangeMax); //get the random number 
	    $newRand = base_convert($base10Rand, 10, 36); //convert it 

	    return $newRand; //spit it out 
	    exit();
	} 
	
	function send_email_verification($email, $verification_code, $id){
			$pr = base64_encode($id);
			$to = $email;
			$link = "http://francisalbores.com/testsite/VideoSpinner/?page=home&vr={$verification_code}&pr={$pr}";
			$subject = 'Complete Registration on vidSpinner.com';
			$message = "<h4>You're almost done!</h4>";
			$message .="<br></br>";
			$message .= "<a href='{$link}'>";
			$message .= "Click here to complete your registration on vidSpinner.com";
			$message .="</a>";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .="After verification you will have.";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .= "This is an automated e-mail message, please do not reply directly. You can contact us by sending an e-mail to support@vidSpinner.com. ";
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: vidSpinner <espiridionlarosa@yahoo.com>' . "\r\n";
			$headers .= 'Bcc: edion.larosa@yahoo.com' . "\r\n";

			// Mail it
			mail($to, $subject, $message, $headers);
			exit();		
	}
	function send_email_reset_pass($email, $code){
			$to = $email;
			$code = $code;
			$link = "http://francisalbores.com/testsite/VideoSpinner?page=home&status=reset";
			$subject = 'Your vidSpinner.com renew password instructions!';
			$message = "<h3>Dear Sir/Madam,</h3>";
			$message .= " <p>You have requested to change your vidSpinner account password. To finish this process, please visit the following link:</p>";
			$message .= "and use your new password as authentication.";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .= "<a href='{$link}'>";
			$message .= "http://umalertsystem.com/VideoSpinner?page=home&status=sdsdsd09sdsd";
			$message .="</a>";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .= "Your New Password: {$code}";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .="If you didn't request any changes, just ignore this message. If you have any other questions regarding your account, please use our FAQ or contact our support team at support@vidSpinner.com.";
			$message .="<br></br>";
			$message .="<br></br>";
			$message .= "This is an automated e-mail message, please do not reply directly. You can contact us by sending an e-mail to support@vidSpinner.com. ";
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: vidSpinner <espiridionlarosa@yahoo.com>' . "\r\n";
		/*	$headers .= 'Bcc: <edion.larosa@>' . "\r\n";*/
			mail($to, $subject, $message, $headers);
			exit();		
	}
	function encryptor($data){
		$encryp=$data;
		$salt= md5($encryp);
		$encrypted_data = md5($encryp.$salt);
		return $encrypted_data;
	}
	function escape_scape($data){
		return mysql_real_escape_string($data);
	}
	function verification_code_generator(){
		// Random confirmation code 
		return $confirm_code=md5(uniqid(rand())); 
	}
	function add(){
		global $user;
		//$errors = array();
		if(isset($_POST['btn-submit'])){
				$data_user = array(
					"first_name"=>$this->escape_scape($_POST['first_name']), "middle_name"=>$this->escape_scape($_POST['middle_name']),
					"last_name"=>$this->escape_scape($_POST['last_name']), 
					"password"=>$this->encryptor($_POST['password']), "email"=>$this->escape_scape($_POST['email']),"avatar"=>"", 
					"verification_code"=>$this->verification_code
				);
				$check_email =  $user->select("email", array("email"=>$data_user['email']),true, "user" );
			if ($check_email!=false){
				session_start();
				$_SESSION['fname'] = $_POST['first_name'];
				$_SESSION['middle'] = $_POST['middle_name'];
				$_SESSION['last'] = $_POST['last_name'];
				$_SESSION['email'] = $_POST['email'];
				header("Location:?page=home&signup_status=ERROR");
				exit();		
			}else{
					if (!isset($_SESSION)) {
						session_start();
					}
					$data = $user->save($data_user);	
					$_SESSION['email'] = $this->escape_scape($_POST['email']);
					$_SESSION['first_name'] = $this->escape_scape($_POST['first_name']);
					$_SESSION['password'] = $this->escape_scape($_POST['password']);
					$_SESSION['id'] =  $user->get_max('user','id');
					$id = $_SESSION['id'];
					header("Location:?page=registration&signup_status=verify_email");
					$this->send_email_verification($_POST['email'], $this->verification_code, $id);
					exit();
				
			}	
		}
	}

	function update(){
		global $user;
		//$errors = array();
		if(isset($_POST['btn_update_social'])){
				$_data = array(
					"first_name"=>$this->escape_scape($_POST['first_name']), "middle_name"=>$this->escape_scape($_POST['middle_name']),
					"last_name"=>$this->escape_scape($_POST['last_name'])	
				);
				session_start();
				$_SESSION['fname'] = $_POST['first_name'];
				$_SESSION['middle'] = $_POST['middle_name'];
				$_SESSION['last'] = $_POST['last_name'];
				$_SESSION['email'] = $_POST['email'];
				if ($_POST['email'] != $_SESSION['email']) {
					$_data['email'] = $this->escape_scape($_POST['email']);
					$check_email =  $user->select("email", array("email"=>$_data['email']),true, "user" );
					if ($check_email!=false){
						session_start();
						$_SESSION['fname'] = $_POST['first_name'];
						$_SESSION['middle'] = $_POST['middle_name'];
						$_SESSION['last'] = $_POST['last_name'];
						$_SESSION['email'] = $_POST['email'];
						header("Location:?page=user_info&update=ERROR");
					}else{
						$data = $user->save($_data,array("id"=>$_SESSION['id']));
						header("Location:?page=user_info&update=success");
					}
				}else{
					$data = $user->save($_data,array("id"=>$_SESSION['id']));
					header("Location:?page=user_info&update=success");
					exit();
			
					
			}		
	
		}
	}	
	function login(){
			$info = array();
			global $user;
			if (isset($_POST['btn-login'])) {
				$user_email = $_POST['user_email'];
				$user_password = $_POST['user_password'];
				$salt= md5($user_password);
		    	$encrypted_pass = md5($user_password.$salt);
				$data = $user->select("*", array("email" => $user_email, "password"=>$encrypted_pass),true );
				if ($data != false) {
					if($data['verification_code'] == 0){
						session_start();
						$_SESSION['id'] = $data['id'];
						$_SESSION['email'] = $data['email'];
						$_SESSION['name'] = $data['first_name'];
						$_SESSION['avatar'] = $data['avatar'];
						header("Location:?page=dashboard");
					}else{
						session_start();
						$_SESSION['email'] = $data['email'];
						$_SESSION['name'] = $data['first_name'];
						$_SESSION['avatar'] = $data['avatar'];
						header("Location:?page=registration&signup_status=verify_email");
					}
				}else{
					header("Location:?page=home&err=USER-INVALID");
				}
			}
	}
	
	function reset_password(){
		global $user;
		if (isset($_POST['btn-reset-password'])) {
			$email = $_POST['user_email'];
			$code = $this->randomAlphaNum(7);
			$pass = $this->encryptor($code);
			$check_email =  $user->select("email", array("email"=>$email),true, "user" );
			if ($check_email!=false){
				$_data = array("password"=>$pass);
				$data = $user->save($_data,array("email"=>$email));
				header("Location:?page=registration&email=ok");
				$this-> send_email_reset_pass($email, $code);
				exit();
			}else{
				header("Location:?page=registration&email=NOT_FOUND");
				exit();
			}
		}
		
	}
	function verify_user(){
		global $user;
		if (isset($_GET['vr'])) {
			$v_code = $_GET['vr'];
			$p_code =  base64_decode($_GET['pr']);
			$data = $user->select("*", array("verification_code" => $v_code),true );
			if ($data != false) {
				$_data = array("verification_code"=>0);
				$data = $user->save($_data,array("id"=>$p_code));
				header("Location:?page=dashboard&verification=success");
				exit();
			}else{
				header("Location:?page=registration&verification=ERROR");
				exit();
			}
			
		}
	}

	function resend_email(){
		global $user;
		if (isset($_GET['signup_status']) && $_GET['signup_status'] == "resend_email") {
				$email = $_SESSION['email'];
				$pass = $_SESSION['password'];
				$code = $this->verification_code;
				$check_email =  $user->select("id", array("email"=>$email, "verification_code"=>0),true, "user" );
				if ($check_email!=false){
					header("Location:?page=registration&email=verified");
					exit();
				}else{
					$_data = array("verification_code"=>$code);
					$data = $user->save($_data,array("email"=>$email));
					header("Location:?page=registration&resend_email=ok");
					$this->send_email_verification($email,$code, $pass);
					exit();
				}
				
		}
	}
	function change_email(){
		global $user;
		if (isset($_POST['btn-change-email'])) {
			$email = $this->escape_scape($_POST['user_email']);
			$code = $this->verification_code;
			$check_email =  $user->select("email", array("email"=>$email),true, "user" );
			if ($check_email!=false){
				header("Location:?page=registration&signup_status=change_email&s=error");
				exit();
			}else{
				$id = $_SESSION['id'];
				$_data = array("email"=>$email);
				$data = $user->save($_data,array("id"=>$id, "verification_code"=>$code));
				$_SESSION['email'] = $email;
				header("Location:?page=registration&signup_status=change_email&s=success");
				$this->send_email_verification($email, $code , $id);
			}
			
		}
	}
	function logout(){
		session_start();
		session_destroy();
		$facebook->destroySession();
		header("Location: ?page=home");
	}

	function select_user(){
		global $user;
		if (isset($_SESSION['email'])) {
			$email = $_SESSION['email'];
			$data = $user->select("*", array("email" => $email ),true );
			if ($data != false) {
				return $data;
				exit();
			}else{
				return false;
			}
		}
	}
	function select_social_media(){
		global $user;
		if (isset($_SESSION['id'])) {
			$data = $user->select("*", array("user_id" => $_SESSION['id']),false, "social_media" );
			if ($data != false) {
				return $data;
				exit();
			}else{
				return false;
			}
		}
	}
	function update_avatar(){
		global $user;
		//$errors = array();
		if(isset($_POST['btn-submit-avatar'])){

			$allowed_ext = array('jpg', 'JPG','jpeg', 'JPEG','png', 'PNG');

		    $file_name =  strtolower($_FILES['avatar_prof']['name']);
		    /*  $file_ext = pathinfo($_FILES['alert_tone']['name'], PATHINFO_EXTENSION);*/
		    $file_ext = strtolower(end(explode('.', $file_name)));
		    $file_size = $_FILES['avatar_prof']['size'];
		    $file_tmp = $_FILES['avatar_prof']['tmp_name'];

            move_uploaded_file($file_tmp,'assets/images/user-avatar/'.$file_name); // '../../tone/'.$file_name
       		$url_path = SITE_URL.'/assets/images/user-avatar/'.$this->escape_scape($file_name);
			$_data = array("avatar"=>$url_path);
			$data = $user->save($_data,array("id"=>$_SESSION['id']));
			$_SESSION['avatar'] = $url_path;
			header("Location:?page=dashboard");
		}
	}
	/* ========= social media =========*/

	function update_social_media(){
		global $user;
		if(isset($_POST['btn_social_edit'])){
			$_data = array(
				"username"=>$this->escape_scape($_POST['username']), "password"=>$this->escape_scape($_POST['password']),
				"url"=>$this->escape_scape($_POST['url'])	
			);
			$data = $user->save($_data,array("id"=>$_POST['id']), "social_media");
			header("Location:?page=user_info&social_update=success");
			exit();
		}
	}	

	function delete_social_media(){
		global $user;
		if (isset($_GET['status'])) {
			if ($_GET['status'] = 'delete_social_account') {
				$id = $_GET['id'];
				$user->delete($id, "social_media");
				header("Location:?page=user_info&social_delete=success");
				exit();
			}	
		}
		
	}

	function script(){
		?>
		<script>
			
		</script>
		<?php
	}	
}