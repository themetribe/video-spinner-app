<?php 
	include("models/cls_facebook.php");
	include("models/cls_ligthOpenId.php");

	$facebook = new Facebook(array(
		  'appId'  => APP_ID,
		  'secret' => APP_SECRET,
		));
	$user_fb = $facebook->getUser();
	class OpenID extends DB{
		function __construct(){
		parent::__construct(); 
		$this->table = "user";
	}
	
	function escape_scape($data){
		return mysql_real_escape_string($data);
	}
	function user_login_fb(){
		global $facebook;
		  return $facebook->getLoginUrl( array(
	                'scope'         => 'email' 
	            )
	  		);
		}
	function get_user_info_fb(){
	    global $facebook;
	    global $user_fb;
	    global $user;
	    if($user_fb){
			try {
			    // Proceed knowing you have a logged in user who's authenticated.
			   $user_profile = $facebook->api('/me');
			   return $user_profile;
			  } catch (FacebookApiException $e) {
			    error_log($e);
			    $user = null;
		  }  		    	
	    }    
	}
	function insert_user_info_fb(){
		 global $facebook;
	     global $user_fb;
	     global $openID;
	     if (!isset($_SESSION)) {
			session_start();
		}
	     if ($user_fb) {
	     	$avatar = "https://graph.facebook.com/{$user_fb}/picture";
	     	$user = $this->get_user_info_fb();
	     	$_data = array("first_name"=>$user['first_name'], "last_name"=>$user['last_name'], "email"=>$user['email'],
	     					"avatar"=>$avatar, "verification_code"=>1, "auth"=>"facebook");
	     	$check_email =  $openID->select("email", array("email"=>$user['email'], "auth"=>"facebook"),true, "user" );
	     	$_SESSION['name'] = $user['first_name'];
			$_SESSION['email'] = $user['email'];
			$_SESSION['avatar'] = $avatar;
			 $_SESSION['auth']= "facebook";
			if ($check_email==false){
				$data = $openID->save($_data);
				$_SESSION['id'] =  $openID->get_max('user','id');
				header("Location:?page=dashboard&user_status=new");
				exit();
			}else{
				$user =   $openID->select("*", array("email"=>$_SESSION['email'], "auth"=>"facebook"),true, "user" );
				$_SESSION['id'] =  $user['id']; 
				$_SESSION['name'] =  $user['first_name']; 
				$_SESSION['avatar'] =  $user['avatar']; 
				header("Location:?page=dashboard");
				exit();
			}
	     }
	}

	function logoutUrl(){
		global $facebook;
		 $param = array('next'=>SITE_URL);
		 $logoutUrl = $facebook->getLogoutUrl( $param);
	}

	/* =============== google auth ===============*/
	function user_login_google(){
		if (isset($_GET['login'])) {
			if ($_GET['login'] == "google") {
				$callback_website_url=SITE_URL;
				$this->googleAuthenticate($callback_website_url);
			}
		}
	}
	
	function googleAuthenticate($callback_website_url) {
	   $openid = new lightopenid("dion");
	   $openid->identity = 'https://www.google.com/accounts/o8/id';
	   $openid->returnUrl = $callback_website_url;
	   $endpoint = $openid->discover('https://www.google.com/accounts/o8/id');

	   $fields ='?openid.ns=' . urlencode('http://specs.openid.net/auth/2.0') .
	             '&openid.return_to=' . urlencode($openid->returnUrl) .
	             '&openid.claimed_id=' . urlencode('http://specs.openid.net/auth/2.0/identifier_select') .
	             '&openid.identity=' . urlencode('http://specs.openid.net/auth/2.0/identifier_select') .
	             '&openid.mode=' . urlencode('checkid_setup') .
	             '&openid.ns.ax=' . urlencode('http://openid.net/srv/ax/1.0') .
	             '&openid.ax.mode=' . urlencode('fetch_request') .
	             '&openid.ax.required=' . urlencode('email,firstname,lastname,profile') .
	             '&openid.ax.type.firstname=' . urlencode('http://axschema.org/namePerson/first') .
	             '&openid.ax.type.lastname=' . urlencode('http://axschema.org/namePerson/last') .
	              '&openid.ax.type.profile=' . urlencode('http://axschema.org/userinfo/last') .
	             '&openid.ax.type.email=' . urlencode('http://axschema.org/contact/email');

	   header('location:'.$endpoint . $fields);
	   // Redirect to google site
	}

	function insert_user_info_google(){
		if (!isset($_SESSION)) {
			session_start();
		}
		 global $openID;
		if (!empty($_GET['openid_ext1_value_firstname']) && !empty($_GET['openid_ext1_value_lastname']) && !empty($_GET['openid_ext1_value_email'])) {
		 // $username = $_GET['openid_ext1_value_firstname'] . $_GET['openid_ext1_value_lastname'];
		 $_SESSION['avatar']=$_GET['openid_ext1_value_profile'];
		 $_SESSION['name']=$_GET['openid_ext1_value_firstname'];
		 $_SESSION['email']=$_GET['openid_ext1_value_email'];
		 $_SESSION['auth']= "google";
		 //$_SESSION['last_name'] = $_GET['openid_ext1_value_lastname'];
		 $_data = array("first_name"=> $_SESSION['name'], "last_name"=>$_SESSION['last_name'], "email"=> $_SESSION['email'],
	     					"avatar"=> $_SESSION['avatar'], "verification_code"=>1, "auth"=>"google");
	     	$check_email =   $openID->select("email", array("email"=>$_SESSION['email'], "auth"=>"google"),true, "user" );
			$_SESSION['avatar'] = "";
			if ($check_email==false){
				$data = $openID->save($_data);
				$_SESSION['id'] =  $openID->get_max('user','id');
				header("Location:?page=dashboard&user_status=new");
				exit();
			}else{
				$user = $openID->select("*", array("email"=>$_SESSION['email'], "auth"=>"google"),true, "user" );
				$_SESSION['id'] =  $user['id']; 
				/*$_SESSION['name'] =  $user['first_name']; */
				$_SESSION['avatar'] =  $user['avatar']; 
				/*$_SESSION['avatar'] =  $user['email']; */

				header("Location:?page=dashboard");
				exit();
			}
		}
	}

	function add_social_acount(){
		global $openID;
		$arr_where=array();
		if (isset($_SESSION['id'])) {
			$user_id = $_SESSION['id'];
		}
		
		if (isset($_POST['btn_submit_social'])) {
			$data = $_POST;
			for ($i=0; $i < count($data['username']); $i++) { 
				$_data = array();
				$_data['user_id'] = $user_id;
				$_data['username'] = $this->escape_scape($data['username'][$i]);
				$_data['password'] = $this->escape_scape($data['password'][$i]);
				$_data['url'] = $this->escape_scape($data['url'][$i]);
				$data_ = $openID->save($_data, $arr_where, "social_media");
			}
			header("location: ?page=dashboard");
			exit();
		}
	}

	function logout(){
		global $facebook;
		if (isset($_GET['status'])) {
			if ($_GET['status'] == 'logout') {
				unset($user_fb);
  			   $facebook->destroySession();
  			   session_destroy();
  			    header("location: ?page=home");
			}
		}
	
	}
}	
 ?>