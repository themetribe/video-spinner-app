<?php
class Project extends DB{
	function __construct(){
		parent::__construct(); 
		$this->table = "project";
		
	}
	function foo(){
		echo "foo";
	}
	function script(){
		?>
		<script>
			
		</script>
		<?php
	}	


	function save_intro_image($id, $image_included){
		$image = json_decode(urldecode($image_included),true);
		foreach($image as $key=>$val){
			unset($arr);
			$arr['project_id']=$id;
			$arr['type']="intro_image";
			$arr['path']=$val;
			//$project->save($arr,"project_media");	
			$project->add_statement($arr,"project_media");
		}
	}
	function save_center_image($id, $image_included){
		$image = json_decode(urldecode($image_included),true);
		foreach($image as $key=>$val){
			unset($arr);
			$arr['project_id']=$id;
			$arr['type']="center_image";
			$arr['path']=$val;
			//$project->save($arr,"project_media");	
			$project->add_statement($arr,"project_media");
		}
	}
	function save_end_image($id, $image_included){
		$image = json_decode(urldecode($image_included),true);
		foreach($image as $key=>$val){
			unset($arr);
			$arr['project_id']=$id;
			$arr['type']="end_image";
			$arr['path']=$val;
			//$project->save($arr,"project_media");	
			$project->add_statement($arr,"project_media");
		}
	}


	function save_music($id, $music_included){
		$music = json_decode(urldecode($music_included),true);
		foreach($music as $key=>$val){
			unset($arr);
			$arr['project_id']=$id;
			$arr['type']="audio";
			$arr['path']=$val;
			//$project->save($arr,"project_media");	
			$project->add_statement($arr,"project_media");
		}
	}

	function get_data($whereParam = array()){
		$data = $this->select("*",$whereParam);
		return $data;			
	}

	function get_new_project_id(){
		$data = $this->query("SHOW TABLE STATUS LIKE  'project'");
		$array = $data->fetchAll(PDO::FETCH_ASSOC);
		//print_r($array);
		//echo "Current: ".$array[0]["Auto_increment"];
		return $array[0]["Auto_increment"];
	}
		
	function create_project($email=""){
		// set default timezone

	}
}