<div id="signup">
	<div class="container">
		<h2>Signup</h2>
		<p>Register to start Spinning Videos</p>
		<?php if (isset($_POST['btn-submit'])): ?>
			<?php $data = add(); ?>
			<?php if (empty($data)): ?>
				<div id='error-avatar' class='alert alert-success'>
					<strong>Successfully Registered</strong></div>
				<?php else: ?>
				<div id='error-avatar' class='alert alert-danger'>
				<?php foreach ($data as $key => $value): ?>
				<strong><?php echo $value; ?></strong> </br>
				<?php endforeach ?>
				</div>
			<?php endif ?>
		<?php endif ?> 
		<form method="POST" enctype="multipart/form-data">
			<div id="personale_info" class="form-horizontal pull-left">
				<h4>Personal Information</h4>
				<div class="form-group">
					<label for="first_name" class="col-sm-3 control-label">First Name:</label>
					<div class="col-sm-9">
				      <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
				    </div>
				</div>
				<div class="form-group">
					<label for="middle_name" class="col-sm-3 control-label">Middle:</label>
					<div class="col-sm-9">
				      <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" required>
				    </div>
				</div>
				<div class="form-group">
					<label for="last_name" class="col-sm-3 control-label">last Name:</label>
					<div class="col-sm-9">
				      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="last Name" required>
				    </div>
				</div>
				<div class="form-group">
					<label for="username" class="col-sm-3 control-label">username:</label>
					<div class="col-sm-9">
				      <input type="text" class="form-control" id="username" name="username"placeholder="Username" required>
				      <img id="tick" src="<?php echo SITE_URL ?>/assets/images/tick.png" width="16" height="16"/>
					  <img id="cross" src="<?php echo SITE_URL ?>/assets/images/cross.png" width="16" height="16"/>
				    </div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-3 control-label">Password:</label>
					<div class="col-sm-9">
				      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
				    </div>

				</div>
				<div class="form-group">
					<label for="email" class="col-sm-3 control-label">Email:</label>
					<div class="col-sm-9">
				      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
				    </div>
				</div>
			</div> <!-- // == div: personal info==-->

			<div id="social_info" class="form-horizontal pull-right">
				<h4>Social Site Info</h4>
				<!-- <h5>Youtube</h5> -->
				<div class="form-group">
					<label for="first_name" class="col-sm-3 control-label">Username:</label>
					<div class="col-sm-9">
				      <input type="text" class="form-control" id="username-social" name="username-social" placeholder="Username" required>
				    </div>
				</div>
				<div class="form-group">
					<label for="middle_name" class="col-sm-3 control-label">password:</label>
					<div class="col-sm-9">
				      <input type="password" class="form-control" id="password-social" name="password-social" placeholder="Password" required>
				    </div>
				</div>
				<div class="form-group">
					<label for="last_name" class="col-sm-3 control-label">Site url:</label>
					<div class="col-sm-9">
				      <input type="url" class="form-control" id="url" name="url" placeholder="Site url" required>
				    </div>
				</div> <br><br>
				<div id="avatar-signup">
					<img id="profile-avatar" src="<?php echo SITE_URL ?>/assets/images/sample-avatar.png" alt="..." class="img-thumbnail">
					<a href="" id="upload_link" style="text-decoration: none;"><h4>Upload avatar</h4></a>
					<div id="error-upload"></div>
				</div>
                <input type="file" class="form-control" id="avatar" name="avatar" style="display: none;">

				 <div clas="form-group">
              		<input type="submit" id="btn-submit" name="btn-submit" class="btn btn-default form-control" value="Register">
           		 </div>   		 
			</div> <!-- // == div: social info ==-->
		</form>		
	</div>
</div> <!-- // == id: signup ==-->
