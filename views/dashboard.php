<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">
			<?php if (isset($_GET['user_status'])): ?>
				<?php if ($_GET['user_status'] == 'new'): ?>

					<h2>We need more information from you.</h2>
					<div id="social-wrapper">
						<h4>Social Media Accounts</h4>
					<!-- 	<hr> -->

						<div class="pull-right">
							<button  id="add_social"> <span class="glyphicon glyphicon-plus"></span></button>
							<!-- <img id="add_social" src="<?php //echo SITE_URL ?>assets/images/social-media-add.png" alt="..." class="img-thumbnail"> -->
						</div>	
						<div class="clear_both"></div>
						<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
						<div class="table-responsive">
						  <table class="table" id="tbl_social">
							<thead>
								<tr>
									<td>Username</td>
									<td>Password</td>
									<td>Url</td>
									<td> </td>
								</tr>
							</thead>
							<TBODY>
								<tr>
									<td><input type='text' class='form-control small' id='username' name='username[]' placeholder='Username' maxlength='25' value='' required/></td>
									<td><input type='text' class='form-control small' id='password' name='password[]' placeholder='Password' maxlength='25' value='' required/></td>
									<td><input type='url' class='form-control large' id='url' name='url[]' placeholder='Account url' maxlength='100' value='' required/></td>
									<td><span class='glyphicon glyphicon-remove btnDelete'></span> </td>
								</tr>
							</TBODY>
						  </table>
						</div>
						<div class="btn-submit-container">
					 		<div class="form-div-btn">
					 			<input type="submit" id="btn-submit" name="btn_submit_social" class="btn btn-info form-control" value="Save">
					 		</div>
			 			</div>	
			 			<hr>		
					</div>
				<?php endif ?>
			<?php else: ?>
				<div class="top">		
				<h3><span class="glyphicon glyphicon-user"></span> My Projects</h3>
				<a class="btn btn-default btn-sm ajax-link" href="?page=add_project">Create New Project</a>
				</div>
			<div id="project-lists">
				<ul>
					<?php
							
						if(count($project_list)>0){
							foreach( $project_list as $key=>$value){
								echo "<li><a href=#>".$value["title"]."</a></li>";
							}
						}
						
					?>
				</ul>
			</div>
			<?php endif ?>
			
		</div>

	</div>
</div>


