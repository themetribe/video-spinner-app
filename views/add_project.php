<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">
			<form name="new-project-form" enctype="multipart/form-data">

				<h1>Add New Project</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet quam non purus condimentum euismod. Suspendisse nec ullamcorper justo.</p>
				<div id="step1" class="step">
					<h2>Step1 <small>- Project Detail</small></h2>
					<div class="button">
						<a href="#" class="btn btn-primary btn-continue">Continue</a>
					</div>
					<div class="project-details">
						<table>
							<tr>
								<td><label for="title">Title</label></td>
								<td><input type="text" name="project-title" /></td>
							</tr>
							<tr>
								<td><label for="website">Website</label></td>
								<td><input type="text" name="project-website" class="input-large search-query"/></td>
							</tr>					
						</table>
					</div>
				</div>

				<div id="step2" class="step">
					<h2>Step 2 <small>- Introduction Images</small></h2>
					<div class="button">
						<a href="#" class="btn btn-primary btn-back">Back</a>
						<a href="#" class="btn btn-primary btn-continue">Continue</a>
					</div>
					<h3>Uploaded Images</h3>	
					<div class="images">
						<div id="image-thumbnails-beginning">
							<span id="browse-beginning-images" class="img-thumbnail">
								<a href="#" class="add-image">+</a>
							</span>
							<?php 
							//print_r($images);
							/*
								foreach($images as $image){
									echo '<img src="'.SITE_URL.$image["path"].'" alt="..." class="img-thumbnail ">';
								}
								*/
							?>
						</div>
						<!--
						<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
						<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
						<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					-->

					</div>
					<input type="file" name="uploaded_beginning_images" multiple="true" id="uploaded_beginning_images" style="display: none;"  onchange="readImageURLBeginning(this)"/><br/>
					<input name="file_beginning" type="text" />
				</div>

				<div id="step3" class="step">
					<h2>Step 3 <small>- Content Images</small></h2>
					<div class="button">
						<a href="#" class="btn btn-primary btn-back">Back</a>
						<a href="#" class="btn btn-primary btn-continue">Continue</a>
					</div>
					<h3>Uploaded Images</h3>	
					<div class="images">
						<div id="image-thumbnails-middle">
							<span id="browse-middle-images" class="img-thumbnail">
								<a href="#" class="add-image">+</a>
							</span>
							<?php 
							//print_r($images);
							/*
								foreach($images as $image){
									echo '<img src="'.SITE_URL.$image["path"].'" alt="..." class="img-thumbnail ">';
								}
								*/
							?>
						</div>					
					</div>
					<input type="file" name="uploaded_middle_images" multiple="true" id="uploaded_middle_images" style="display: none;"  onchange="readImageURLMiddle(this)"/><br/>
					<!--<input type="file" name="uploaded_middle_images" multiple="true" id="uploaded_middle_images" style="display: none;" /><br/>-->
					<input name="file_middle" type="text" />

				</div>

				<div id="step4" class="step">
					<h2>Step 4 <small>- Final Images</small></h2>
					<div class="button">
						<a href="#" class="btn btn-primary btn-back">Back</a>
						<a href="#" class="btn btn-primary btn-continue">Continue</a>
					</div>
					<h3>Uploaded Images</h3>	
					<div class="images">
						<div id="image-thumbnails-final">
							<span id="browse-final-images" class="img-thumbnail">
								<a href="#" class="add-image">+</a>
							</span>
							<?php 
							//print_r($images);
							/*
								foreach($images as $image){
									echo '<img src="'.SITE_URL.$image["path"].'" alt="..." class="img-thumbnail ">';
								}
								*/
							?>
						</div>

					</div>
					<input type="file" name="uploaded_final_images" multiple="true" id="uploaded_final_images" style="display: none;"  onchange="readImageURLFinal(this)"/><br/>
					<input name="file_final" type="text" />

				</div>
				<div id="step4" class="step">
					<h2>Step 5 <small>- Music</small></h2>
					<div class="button">
						<a href="#" class="btn btn-primary btn-back">Back</a>
						<a href="#" class="btn btn-primary btn-continue">Continue</a>
					</div>
					<h3>Music</h3>	
					<table class="table table-bordered">

					<?php
					$media_dir = ROOT_DIR."\assets\media\music_library\\";
					if ($handle = opendir($media_dir)) {
					    while (false !== ($entry = readdir($handle))) {
					    	if($entry=='.'||$entry=='..') continue;
					?>
						<tr>
							<td><audio src="<?php echo SITE_URL.'/assets/media/music_library/'.$entry ?>" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
							<td><label for="music1"><?php echo $entry; ?></label></td>
							<td><input type="checkbox" name="music[]" value="<?php echo $entry; ?>" id="<?php echo $entry; ?>"></td>
						</tr>
					<?php
					    }
					}

					?>					

					</table>
				</div>
			</form>

		</div>


	</div>
</div>