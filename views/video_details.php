<?php
$music_included = json_encode($_POST['music'],JSON_FORCE_OBJECT);
?>

<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">
			<h1>Video Details</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet quam non purus condimentum euismod. Suspendisse nec ullamcorper justo.</p>
			<div class="button">
				<a href="#add_music" class="btn btn-primary ajax-link">Back</a>
				<a href="#" class="btn btn-primary btn-submit" data-form-id="frmvideo_details">Continue</a>
			</div>
			<h2>Step5 <small>- Video Details</small></h2></br>

			<form id="frmvideo_details" method="POST" class="form-horizontal" role="form" action="?page=post_to">
				<input type="hidden" name="music_included" value="<?php echo $music_included ?>" />

				<div class="form-group">
					<label for="num_of_videos" class="col-sm-2 control-label"># of Videos</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="num_of_videos" name="num_of_videos" placeholder="# of Videos">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-2 control-label">Description</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="description" name="description" placeholder="Video description">
						<p class="text-muted">Press ',' to enter more description.</p>
					</div>
				</div>
				<div class="form-group">
					<label for="tags_keywords" class="col-sm-2 control-label">Tags/Keywords</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="tags_keywords" name="tags_keywords" placeholder="Tags/Keywords">
					</div>
				</div>
			</form>

		</div><!--main -->
	</div>
</div>

