<?php
  error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
?>
	<div id="top-area" class="vcenter">
	<div>
		<h1>VidSpin</h1> <?php /*session_destroy();*/ //echo $_SESSION['email'];?> 
		<p>An awesome video spinner for your oh so awesome site!</p><br />
		<a href="#" class="btn btn-lg">Get Started!</a>
	</div>
</div>
<div id="features">
	<div class="container">
		<h2>Features and What We Offer</h2>
		<div class="row">
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/mobile-device-compatibility.png" />
				<h4>Mobile Device Compatibility</h4>
				<p>Create multiple videos at one time right from your mobile device, and upload them to YouTube automatically.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/free-music-library.png" />
				<h4>Royalty Free Music Library</h4>
				<p>Use our unique royalty free music library to enhance your videos and make them unique and interesting.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/tags-names.png" />
				<h4>Automatic Tags & Naming</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/facebook-twitter-youtube.png" />
				<h4>Automatic YouTube, Facebook, Instagram Uploader</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>

			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/caption-generator.png" />
				<h4>Caption Generator</h4>
				<p>Insert texts/captions/labels on each of the images uploaded.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/automatic-geo-targeting.png" />
				<h4>Automatic Geo Targeting</h4>
				<p>Improve and Dominate your local search by Automatically Geo Targeting your videos.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/keyword-helper.png" />
				<h4>Automatic Keyword Helper Tool</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/video-generator.png" />
				<h4>"50 in 5" Video Generator</h4>
				<p>Build 50 videos in 5 minutes, or less. How many Search Engine Optimized Business Building Videos could you get on YouTube and get ranking for in 5 minutes?</p>
			</div>
		</div>
	</div>
</div>
<div id="our-team">
	<div class="container">
		<h2>Our Team</h2>
		<p><strong>"Nah! This isn't the real we. I just put this as a temporary cous they look oh so professional and cool." - Francis "the real me"</strong></p>
		<small>The Web Artisans</small>
		<img src="assets/images/team-b.jpg" />
	</div>
</div>
<div id="pricing" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Pricing" aria-hidden="true">
	<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	<div class="container">
		<h2>Pricing Options</h2>
		<p>Chose the option that best fits your needs</p>
		<table class="table table-striped">
			<tr>
				<td width="350">&nbsp;</td>
				<th>Starter</th>
				<th>Basic</th>
				<th width="200">Pro</th>
			</tr>
			<tr>
				<td><em>License</em></td>
				<td>Free</td>
				<td>$69</td>
				<td>$690</td>
			</tr>
			<tr>
				<td><em>Frequency</em></td>
				<td>&infin;</td>
				<td>Monthly</td>
				<td>Yearly</td>
			</tr>
			<tr>
				<td><em>Auto Submit to Social Media</em></td>
				<td>No</td>
				<td>Yes</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td><em>Limitation</em></td>
				<td>Up to 10 Image Upload limit per section (intro, mid, ending)</td>
				<td>Up to 5 Video Spins per Project</td>
				<td>&infin;</td>
			</tr>
			<tr>
				<td><em>Can Embed Music from Library</em></td>
				<td>Yes</td>
				<td>Yes</td>
				<td>Yes</td>
			</tr>

		</table>
	</div>
</div></div></div>
</div>


<div class="modal fade" id="user_login" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
	<div id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h2>Sign In</h2> 
					<p>Lorem ipsum dolor sit amet consectetur.</p>
					<form class="form-horizontal" role="form" method="post">
						<div class="form-div">
							 <?php if (isset($_GET['err'])): ?>
							 	<div class="alert alert-danger">Invalid Login. Please try again.</div>
							 <?php endif ?>
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_email">Email address</label>
								<input class="form-control" type="email" name="user_email" id="txt_user_email" placeholder="your-email@domain.com" />
							</div>
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_password">Password</label>
								<input class="form-control" type="password" name="user_password" id="txt_user_password" placeholder="Password here..." />
							</div>
						</div>
						<input type="submit" value="Login" name="btn-login" id="btn-login" class="btn btn-primary btn-lg" />
						<div class="redirect-message"></div>
						<div class="pop"></div>
						<p><a href="?page=remind_password" id="forgot-password">Forgot your Password?</a></p>
						<div id="login-alternative">
							<p>Alternatively, sigin with </p>
							<a href="<?php echo $loginUrl; ?>" id="login_fb" class="text-success" rel="popover" data-content="Redirecting to Facebook!" data-placement="left"><img src="<?php echo SITE_URL ?>/assets/images/login-fb.png" alt="Facebook"/></a>
							<a href="?page=home&login=google" id="login_google" class="text-success" rel="popover" data-content="Redirecting to Google!" data-placement="right"><img src="<?php echo SITE_URL ?>/assets/images/login-google.png" alt="Google"/></a>
						</div>
					</form> 
				</div>			
			</div>
		</div>
	</div>
	<div id="modal-forgot-pass" class="hidden">
		<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	        		<button type="button" class="close" id="close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<div class="forgot-pass-wrapper">
	        			<form class="form-horizontal" role="form" method="post">
			        		<h4>Reset Password</h4> 
							<div class="yellow-ribbon">
									<p><strong>Forgot password?</strong> Enter your email address</p>
								
							</div>
							<form class="form-horizontal" role="form" method="post">
							<div class="form-div">
								<div class="form-group">
				    				<label class="sr-only" for="txt_user_email">Email address</label>
				    				<div class="col-sm-10">
				    					<input class="form-control" type="email" name="user_email" id="txt_user_email" placeholder="your-email@domain.com" />
				    				</div>
									<input type="submit" value="Send" name="btn-reset-password" id="btn-reset-password" class="btn btn-primary " />
								</div>
							</div>
						</form> 
	        		</div>
				</div>			
			</div>
		</div>
	</div>
</div> <!-- ===== // id: user_login modal ====== -->
<div class="modal fade" id="reset-password" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
	<div id="modal-forgot-pass">
		<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	        		<button type="button" class="close" id="close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
	        		<div class="forgot-pass-wrapper">
		        		<h4>Reset Password</h4> 
						<div class="yellow-ribbon">
							<div style="margin-top: 10px">
								<strong><p>Please login using your new password.</p></strong>
							</div>
						</div>
						<form class="form-horizontal" role="form" method="post">
						<div class="form-div">
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_email">Email address</label>
			    					<input class="form-control" type="email" name="user_email" id="txt_user_email" placeholder="your-email@domain.com" value="<?php echo isset($_SESSION['email'])? $_SESSION['email']:""; ?>"/>
							</div>
						</div>
						<div class="form-div">
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_email">Your new password</label>
			    				<input class="form-control" type="email" name="user_email" id="txt_user_email" placeholder="your-new password" />
							</div>
						</div>
						<div clas="form-div-btn">
							<input type="submit" value="Login" name="btn-login" id="btn-login" class="btn btn-primary " />
						</div>
						</form> 
	        		</div>
	        		
				</div>			
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
	<div id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h2>VidSpin Registration</h2> 
					<hr>
					<div id="signup-wrapper">
						<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
							<div id="personal-info">
									<div class="center">
										<?php if (isset($_GET['signup_status'])): ?>
											<?php if ($_GET['signup_status'] == "ERROR"): ?>
												<div class='alert alert-danger'>
													<p>The email is already registered in the system, <br>please use another email address.</p>
												</div>
											<?php endif ?>
										<?php endif ?> 
									</div>
								<div class="form-div">
										<div class="form-group">
											<label for="first_name" class="col-sm-3 control-label sr-only">First Name:</label>
										    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" maxlength="15" value="<?php echo isset($_SESSION['fname'])? $_SESSION['fname']:""; ?>" required>
										</div>
										<div class="form-group">
											<label for="middle_name" class="col-sm-3 control-label sr-only">Middle:</label>
										    <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" maxlength="15" value="<?php echo isset($_SESSION['middle'])? $_SESSION['middle']:""; ?>" required>
										</div>
										<div class="form-group">
											<label for="last_name" class="col-sm-3 control-label sr-only">last Name:</label>
										    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="last Name" maxlength="15" value="<?php echo isset($_SESSION['last'])? $_SESSION['last']:""; ?>" required>
										</div>
										<div class="form-group">
											<label for="password" class="col-sm-3 control-label sr-only">Password:</label>
										      <input type="password" class="form-control" id="password" name="password" placeholder="Password" maxlength="15" value="" required>
										      <div id="error-pass-length" class="hidden">
										      	 <p class="error">Password is too short (minimum is 6 characters).</p>
										      </div>
										</div>
										<div class="form-group">
											<label for="password" class="col-sm-3 control-label sr-only">Password:</label>
										      <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="Confirm password" maxlength="15" value="" required>
										      <div id="error-pass" class="hidden">
										      	 <p class="error">Your passwords don't match.</p>
										      </div>
										      <div id="success-password" class="hidden">
										      	 <p class="success">Passwords ok.</p>
										      </div>
										     
										</div>

										<div class="form-group">
											<label for="email" class="col-sm-3 control-label sr-only">Email:</label>
										      <input type="email" class="form-control" id="email" name="email" placeholder="your-email@domain.com" value="<?php echo isset($_SESSION['email'])? $_SESSION['email']:""; ?>" required>
										</div>
										<div id="btn-submit-container">
									 		<div class="form-div-btn">
									 			<input type="submit" id="btn-submit" name="btn-submit" class="btn btn-primary form-control" value="Submit">
									 		</div>
							 			</div>
							 			<div class="label-privacy">
									 			<p>By clicking Submit you acknowledge that you have read and understood the <a href="#">Privacy Policy</a> of vidSpinner</p>
								 			</div>
									</div> <!-- =====/id: form-div -->
							</div> <!-- ===== // id: "personal-info" =====-->
						<hr>
					</div>	
				</div>			
			</div>
		</div>
	</div>
</div>
