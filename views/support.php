<div id="support_page">
	<div class="container">
		<h2>Support</h2>
		<p>All resources you need in getting started</p>

		<div class="row">			
			<div class="col-md-3 left">
				<img src="<?php echo SITE_URL ?>/assets/images/faq.png" />
				<h3>FAQ</h3>
				<p>Read our most frequently asked questions about us and our themes.</p>
				<a class="btn btn-default">Visit FAQ</a>
			</div>
			<div class="col-md-6" id="contact-form">
				<h3>Unsure? Asks us...</h3>				
				<p>Do you have questions about our service that you would like to clarify prior to purchase. Go right ahead and fill in the form to get in touch and we will get back as soon as possible.</p>
				<p><input type="text" class="form-control" placeholder="Francis"></p>
				<p><input type="text" class="form-control" placeholder="my@email.com"></p>
				<p><input type="text" class="form-control" placeholder="Your question is about..."></p>
				<p><textarea class="form-control" rows="5" placeholder="Your question details..."></textarea></p>
				<p><input type="submit" class="form-control btn btn-primary" value="Submit" /></p>
			</div>
			<div class="col-md-3 right">
				<img src="<?php echo SITE_URL ?>/assets/images/documentation.png" />
				<h3>Documentation</h3>
				<p>Have a look at our rich documentation and tutorials to help you get started.</p>
				<a class="btn btn-default">Read To Get Started</a>
			</div>
		</div>
		<div class="row">
			
		</div>
	</div>
</div>