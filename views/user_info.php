<div id="user-info">
	<div class="header">
			<h2>Manage account information</h2>
	</div>
	<h5>Edit personal details</h5>
	<div class="container">
		<div class="personal-info">
			
				<?php if (isset($_GET['update'])): ?>
					<?php if ($_GET['update'] == 'ERROR'): ?>
						<div class="alert alert-danger">Email already used.</div>
					<?php else: ?>
					<div class="alert alert-success">Account successfully updated.</div>
					<?php endif ?>
				<?php endif ?>
	
			<form class="form-horizontal" role="form" method="POST">
				 <fieldset>
				<div class="form-group">
					<!-- <label for="first_name" class="col-sm-3 control-label">First Name:</label> -->
					<div class="col-sm-9">
						<input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" maxlength="15" value="<?php echo isset($_SESSION['fname'])? $_SESSION['fname']: $user_info['first_name']; ?>" required>
					</div>
					</div>	
				<div class="form-group">
					<!-- <label for="middle_name" class="col-sm-3 control-label">Middle:</label -->
					<div class="col-sm-9">
				   		<input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" maxlength="15" value="<?php echo isset($_SESSION['middle'])? $_SESSION['middle']: $user_info['middle_name']; ?>" required>   
				   	</div>	
				</div>
				<div class="form-group">
				<!-- 	<label for="last_name" class="col-sm-3 control-label">last Name:</label> -->
					<div class="col-sm-9">
				    	<input type="text" class="form-control" id="last_name" name="last_name" placeholder="last Name" maxlength="15" 	value="<?php echo isset($_SESSION['last'])? $_SESSION['last']:  $user_info['last_name']; ?>" required>
					</div>
				</div>
				<div class="form-group">
					<!-- <label for="email" class="col-sm-3 control-label">Email:</label> -->
					<div class="col-sm-9">
						<input type="email" class="form-control" id="email" name="email" placeholder="your-email@domain.com" value="<?php echo isset($_SESSION['email'])? $_SESSION['email']: $user_info['email']; ?>" <?php if (isset($_SESSION['auth'])) echo "disabled"; ?>  required>
				    </div>
				</div>
				</fieldset>
				<div class="form-group">
				    <div class="col-sm-9">
						<input type="submit" id="btn_update_social" name="btn_update_social" class="btn btn-primary form-control" value="Save Changes">
				    </div>
				</div> 
	 		</form> 	
	 	</div>	 <!-- ===== id: personal-info -->

	 		<hr style="width: 60%;">
				<h5>Social Media account</h5>
			<div class="social-info">
					<div class="alert-container">
						<?php if (isset($_GET['social_delete'])): ?>
							<?php if ($_GET['social_delete'] == 'success'): ?>
								<div class="alert alert-success">Social media account successfully deleted.</div>
							<?php endif ?>
						<?php endif ?>
					</div>	
					<div class="alert-container">
						<?php if (isset($_GET['social_update'])): ?>
							<?php if ($_GET['social_update'] == 'success'): ?>
								<div class="alert alert-success">Social media account successfully deleted.</div>
							<?php endif ?>
						<?php endif ?>
					</div>
					<div class="alert-container">
						<div id="alert-sure-delete" class="alert alert-danger">
					      <h4>Are you sure you want to delete this record?</h4>
					      <a class="btn btn-danger yes" href="#">Yes, delete this one please</a> <a class="btn btn-default" href="#">Cancel</a>
					    </div>
					</div>
					 <table class="table table-striped" id="social_table">
						<THEAD>
							<tr>
								<th>Username</th>
								<th>Password</th>
								<th>Url</th>
								<th>Action</th>
							</tr>
						</THEAD>
						<TBODY>
							<?php if ($social_media !=false): ?>
								<?php foreach ($social_media as $key => $value): ?>
									<tr>
										<td class="id hidden"><?php echo $value['id']; ?></td>
										<td class="username"><?php echo $value['username']; ?></td>
										<td class="password"><?php echo $value['password']; ?></td>
										<td class="url"><?php echo $value['url']; ?></td>
										<td>
											<a href="#" class="a-social-edit" data-toggle="modal" data-target="#social_media"><span class="glyphicon glyphicon-edit"></span></a>
											<a href="#" class="a-social-delete" style="margin-left: 20px;"><span class="glyphicon glyphicon-remove"></span></a>

										</td>
									</tr>
								<?php endforeach ?>
							<?php endif ?>
							
						</TBODY>
					</table>
			</div> <!-- ===== // id: social-info ===== -->
	</div>
</div>

<div class="modal fade" id="social_media" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
	<div id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">			
	        	<div class="modal-body">
	        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4>Edit social media account</h4> 
					<form class="form-horizontal" role="form" method="post">
						<div class="form-div">
							<input class="form-control" type="hidden" name="id" id="id" placeholder="id"/>
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_email">Username</label>
								<input class="form-control" type="text" name="username" id="username" placeholder="Username" />
							</div>
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_password">Password</label>
								<input class="form-control" type="password" name="password" id="password" placeholder="Password" />
							</div>
							<div class="form-group">
			    				<label class="sr-only" for="txt_user_password">url</label>
								<input class="form-control" type="url" name="url" id="url" placeholder="Paste your url here..." />
							</div>
						</div>
						<div class="social-button">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      						<button type="submit" name="btn_social_edit"class="btn btn-primary">Save changes</button>
						</div>
						
					</form> 
				</div>			
			</div>
		</div>
	</div>
</div> <!-- ===== // id: user_login modal ====== -->