<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">
			<!--If new project-->
			<h1>Add Projects</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet quam non purus condimentum euismod. Suspendisse nec ullamcorper justo.</p>
			<div class="button">
				<a href="#add_image" class="btn btn-primary ajax-link">Back</a>
				<a href="#" class="btn btn-primary btn-submit" data-form-id="frmadd_music">New Project</a>
			</div>
			<h2>Your Projects <small>- Click a Project to update</small></h2>

			<table class="table table-bordered">
				<?php 
				if($project_list){
					foreach($project_list as $key=>$val) : ?>
						<tr>
							<td><p><?php echo $val['title']; ?></p></td>
						</tr>
					<?php endforeach; } ?>
				<?php 
				?>
			</table>

		</div><!-- main -->
	</div>
</div>



