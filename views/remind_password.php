<div id="top-area" class="vcenter">
	<div>
		<h1>VidSpin</h1>
		<p>An awesome video spinner for your oh so awesome site!</p><br />
		<a href="#" class="btn btn-lg">Get Started!</a>
	</div>
</div>
<div id="features">
	<div class="container">
		<h2>Features and What We Offer</h2>
		<div class="row">
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/mobile-device-compatibility.png" />
				<h4>Mobile Device Compatibility</h4>
				<p>Create multiple videos at one time right from your mobile device, and upload them to YouTube automatically.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/free-music-library.png" />
				<h4>Royalty Free Music Library</h4>
				<p>Use our unique royalty free music library to enhance your videos and make them unique and interesting.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/tags-names.png" />
				<h4>Automatic Tags & Naming</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/facebook-twitter-youtube.png" />
				<h4>Automatic YouTube, Facebook, Instagram Uploader</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>

			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/caption-generator.png" />
				<h4>Caption Generator</h4>
				<p>Insert texts/captions/labels on each of the images uploaded.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/automatic-geo-targeting.png" />
				<h4>Automatic Geo Targeting</h4>
				<p>Improve and Dominate your local search by Automatically Geo Targeting your videos.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/keyword-helper.png" />
				<h4>Automatic Keyword Helper Tool</h4>
				<p>Finding quality and abundance of keywords to target has never been easier or faster. Let our Keyword Helper Tool help you rise to the top.</p>
			</div>
			<div class="col-md-3 col-xs-6 feature">
				<img src="<?php echo SITE_URL ?>/assets/images/video-generator.png" />
				<h4>"50 in 5" Video Generator</h4>
				<p>Build 50 videos in 5 minutes, or less. How many Search Engine Optimized Business Building Videos could you get on YouTube and get ranking for in 5 minutes?</p>
			</div>
		</div>
	</div>
</div>
<div id="our-team">
	<div class="container">
		<h2>Our Team</h2>
		<p><strong>"Nah! This isn't the real we. I just put this as a temporary cous they look oh so professional and cool." - Francis "the real me"</strong></p>
		<small>The Web Artisans</small>
		<img src="assets/images/team-b.jpg" />
	</div>
</div>
<div id="pricing">
	<div class="container">
		<h2>Pricing Options</h2>
		<p>Chose the option that best fits your needs</p>
		<table class="table table-striped">
			<tr>
				<td width="350">&nbsp;</td>
				<th>Starter</th>
				<th>Basic</th>
				<th width="200">Pro</th>
			</tr>
			<tr>
				<td><em>License</em></td>
				<td>Free</td>
				<td>$69</td>
				<td>$690</td>
			</tr>
			<tr>
				<td><em>Frequency</em></td>
				<td>&infin;</td>
				<td>Monthly</td>
				<td>Yearly</td>
			</tr>
			<tr>
				<td><em>Auto Submit to Social Media</em></td>
				<td>No</td>
				<td>Yes</td>
				<td>Yes</td>
			</tr>
			<tr>
				<td><em>Limitation</em></td>
				<td>Up to 10 Image Upload limit per section (intro, mid, ending)</td>
				<td>Up to 5 Video Spins per Project</td>
				<td>&infin;</td>
			</tr>
			<tr>
				<td><em>Can Embed Music from Library</em></td>
				<td>Yes</td>
				<td>Yes</td>
				<td>Yes</td>
			</tr>

		</table>
	</div>
</div>

<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="User Login Modal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">			
        	<div class="modal-body">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        		<div class="forgot-pass-wrapper">
	        		<h4>Forgot password?</h4> 
					<div class="forgot-pass vcenter">
						<p><strong>Forgot password?</strong> Enter your email address</p>
					</div>
					<form class="form-horizontal" role="form" method="post">
					<div class="form-div">
						<div class="form-group">
		    				<label class="sr-only" for="txt_user_email">Email address</label>
		    				<div class="col-sm-10">
		    					<input class="form-control" type="email" name="user_email" id="txt_user_email" placeholder="your-email@domain.com" />
		    				</div>
							<input type="submit" value="Send" name="btn-login" id="btn-login" class="btn btn-primary " />
						</div>
					</div>
					
					</form> 
        		</div>
        		
			</div>			
		</div>
	</div>
</div>
