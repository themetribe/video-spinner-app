<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">

			<h1>Insert Music</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet quam non purus condimentum euismod. Suspendisse nec ullamcorper justo.</p>
			<div class="button">
				<a href="#add_image" class="btn btn-primary ajax-link">Back</a>
				<a href="#" class="btn btn-primary btn-submit" data-form-id="frmadd_music">Continue</a>
			</div>
			<h2>Step4 <small>- Select Soundtracts</small></h2>

			<form id="frmadd_music" role="form" method="POST" action="?page=video_details">
			<table class="table table-bordered">
				<tr>
					<td><audio src="<?php echo SITE_URL ?>/assets/media/music1.mp3" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
					<td><label for="music1">Music 1</label></td>
					<td><input type="checkbox" name="music[]" value="music1" id="music1"></td>
				</tr>
				<tr>
					<td><audio src="<?php echo SITE_URL ?>/assets/media/music2.mp3" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
					<td><label for="music2">Music 2</label></td>
					<td><input type="checkbox" name="music[]" value="music2" id="music2"></td>
				</tr>
				<tr>
					<td><audio src="<?php echo SITE_URL ?>/assets/media/music3.mp3" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
					<td><label for="music3">Music 3</label></td>
					<td><input type="checkbox" name="music[]" value="music3" id="music3"></td>
				</tr>
				<tr>
					<td><audio src="<?php echo SITE_URL ?>/assets/media/music4.mp3" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
					<td><label for="music4">Music 4</label></td>
					<td><input type="checkbox" name="music[]" value="music4" id="music4"></td>
				</tr>
				<tr>
					<td><audio src="<?php echo SITE_URL ?>/assets/media/music5.mp3" data-toggle="tooltip" title="preview" controls>  Audio not supported</audio></td>
					<td><label for="music5">Music 5</label></td>
					<td><input type="checkbox" name="music[]" value="music5" id="music5"></td>
				</tr>
			</table>
			</form>

		</div><!-- main -->
	</div>
</div>



