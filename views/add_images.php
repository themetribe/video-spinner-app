<div id="dashboard" class="container">
	<div class="row">
		<?php include('parts/sidebar.php'); ?>

		<div id="main">




			<h1>Add Image</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet quam non purus condimentum euismod. Suspendisse nec ullamcorper justo.</p>

			<div id="step1" class="step">
				<div class="button">
					<a href="#" class="btn btn-primary btn-continue">Continue</a>
				</div>
				<h2>Step1 <small>- Introduction Images</small></h2>
				<h3>Uploaded Images</h3>	
				<div class="images">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<span class="img-thumbnail"><a href="#" class="add-image">+</a></span>
				</div>
			</div>

			<div id="step2" class="step">
				<div class="button">
					<a href="#" class="btn btn-primary btn-continue">Continue</a>
				</div>
				<h2>Step2 <small>- Content Images</small></h2>
				<h3>Uploaded Images</h3>	
				<div class="images">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<span class="img-thumbnail"><a href="#" class="add-image">+</a></span>
				</div>
			</div>

			<div id="step3" class="step">
				<div class="button">
					<a href="?page=add_music" class="btn btn-primary ajax-link">Continue</a>
				</div>
				<h2>Step3 <small>- Final Images</small></h2>
				<h3>Uploaded Images</h3>	
				<div class="images">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<img src="<?php echo SITE_URL ?>/assets/images/image-sample.png" alt="..." class="img-thumbnail ">
					<span class="img-thumbnail"><a href="#" class="add-image">+</a></span>
				</div>
			</div>



		</div>

	</div>
</div>