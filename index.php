<?php 
session_start();
include("assets/system-functions.php");
Func::init();

include("config.php");

// INCLUDE DB FOR GLOBAL just because it's db.
include("models/cls_db.php");	
include('parts/header.php');
if(file_exists("controllers/{$current_page}.php")){
	include("controllers/{$current_page}.php");	
	 if(isset($_POST['func'])) {
	 	$_POST['func']();
	 }
}		
include("views/{$current_page}.php");
include('parts/footer.php'); 
Func::execute_footer_hook();

?>