<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vidspindb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&49@3A(mwCV%Pf)ufwjNk+o;w$>jn~T~|ah(C_`.K{wLVC757%,dCoWm_SK6K[w!');
define('SECURE_AUTH_KEY',  'LoHzePN#:2D5zhStMr?x@jy8-?Ary/B>C.LPl*ZDAt}$nuW5INM$xjI085? OqBH');
define('LOGGED_IN_KEY',    '.%VHj8>S,jXkH^J&omY#jFzswNs$(c$Au9.#>@U1/so  o&I_EGk<yQ;?O^:;*r#');
define('NONCE_KEY',        'dhAD1L!c=cWYJvrTF>R!Xm(*Y1FTPI,tp8t2~[/~qi5kc5d0{^mnn+w~~r @xQ)o');
define('AUTH_SALT',        'EFpv~P!z@w?mIaW2+KgP2S[McWAj8Cf@zLIBi;yLMDGb1+}fRFrx5Ef|T0ni>gs=');
define('SECURE_AUTH_SALT', 'j_IP:.:2Po-_wHx[!/]b!.lRM*}MHub^{(@dav0!0X.kPIg]G1jV:7h+Q06&|C2K');
define('LOGGED_IN_SALT',   'R!YS>SXBnjGqJa<#x7(TvZxehv`T9)U^Kp]^HyX?:ECX~MDxXno$HhtiwMmaL6&X');
define('NONCE_SALT',       'xRq{B?K=RkF+hZrd33FDhB{soS[QO@b2`7@@cNYNxvD=|;QzN6i@&uphwgT2mkTm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
