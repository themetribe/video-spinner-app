-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2014 at 07:09 AM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vidspindb`
--

-- --------------------------------------------------------

--
-- Table structure for table `generated_videos`
--

CREATE TABLE IF NOT EXISTS `generated_videos` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `url` varchar(512) NOT NULL,
  `keywords` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `generated_videos`
--


-- --------------------------------------------------------

--
-- Table structure for table `music_library`
--

CREATE TABLE IF NOT EXISTS `music_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `url` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `music_library`
--

INSERT INTO `music_library` (`id`, `title`, `url`) VALUES
(1, 'music1', 'music1.mp3'),
(2, 'music2', 'music2.mp3'),
(3, 'music3', 'music3.mp3'),
(4, 'music4', 'music4.mp3'),
(5, 'music5', 'music5.mp3');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `website` varchar(512) DEFAULT NULL COMMENT 'url of the website that the user wants to promote or link to from the videos',
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `title`, `date`, `website`, `email`) VALUES
(1, 'Sample Project 1', '2013-11-20', '', ''),
(2, 'Sample Project 2', '2013-11-21', NULL, ''),
(3, 'test', '0000-00-00', 'test', '');

-- --------------------------------------------------------

--
-- Table structure for table `project_media`
--

CREATE TABLE IF NOT EXISTS `project_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL COMMENT 'types: audio, intro_image, center_image, end_image',
  `path` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `project_media`
--

INSERT INTO `project_media` (`id`, `project_id`, `type`, `path`) VALUES
(1, 4, 'intro_image', '/assets/images/image-sample.png');

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE IF NOT EXISTS `social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(512) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `social_media`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `avatar` varchar(512) NOT NULL,
  `verification_code` varchar(50) NOT NULL,
  `auth` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=216 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `password`, `last_name`, `first_name`, `middle_name`, `email`, `avatar`, `verification_code`, `auth`) VALUES
(204, '1a49b7c505fc92f7287319f35f984dcf', 'dion', 'Dion', 'Dion', 'edion.larosa@yahoo.com', '', '169f184766ebd693a8aa58046a49a786', NULL),
(211, '', 'Rojo', 'Mico', NULL, 'mico.rojo@yahoo.com', 'https://graph.facebook.com/100002994814424/picture', '1', NULL),
(212, '', 'Larosa', 'Edion', NULL, 'edion.larosa@gmail.com', '', '1', NULL),
(214, '', 'Emberda', 'Eric', NULL, 'eric.emberda@gmail.com', '', '1', 'google'),
(215, '', '', 'Waverly', NULL, 'homesforsaledmv10@gmail.com', '', '1', 'google');
