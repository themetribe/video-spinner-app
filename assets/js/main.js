/***********************Image Browser *****************************/
			// set up variables
			var readerBeginning = new FileReader(),
				readerMiddle = new FileReader(),
				readerFinal = new FileReader(),
				i=0,j=0,k=0,
				numFilesBeginning = 0,
				numFilesMiddle = 0,
				numFilesFinal = 0,
				imageFilesBeginning,
				imageFilesMiddle,
				imageFilesFinal;

			// use the FileReader to read image i
			function readFileBeginning() {
				readerBeginning.readAsDataURL(imageFilesBeginning[i]);
			}
			function readFileMiddle() {
				console.log("readFileMiddle");
				readerMiddle.readAsDataURL(imageFilesMiddle[j]);
				console.log("done readFileMiddle");
			}
			function readFileFinal() {
				readerFinal.readAsDataURL(imageFilesFinal[k]);
			}
			// define function to be run when the File
			// reader has finished reading the file
			readerBeginning.onloadend = function(e) {

				// make an image and append it to the div
				console.log("readerBeginning.onloadend");
				//var image = $('<img>').attr('src', e.target.result);
				var image = $('<img>').attr({
					src:e.target.result,
					class:"img-thumbnail",
					width:"140px",
					height:"140px"
				});
				$(image).appendTo('#image-thumbnails-beginning');

				// if there are more files run the file reader again
				if (i < numFilesBeginning) {
					i++;
					readFileBeginning();
				}
				console.log("end readerBeginning.onloadend");

			};

			readerMiddle.onloadend = function(e) {

				// make an image and append it to the div
				console.log("readerMiddle.onloadend");
				//var image = $('<img>').attr('src', e.target.result);
				var image = $('<img>').attr({
					src:e.target.result,
					class:"img-thumbnail",
					width:"140px",
					height:"140px"
				});
				$(image).appendTo('#image-thumbnails-middle');

				// if there are more files run the file reader again
				if (j < numFilesMiddle) {
					j++;
					readFileMiddle();
				}
				
				console.log("done onload readerMiddle");
			};

			readerFinal.onloadend = function(e) {

				// make an image and append it to the div

				//var image = $('<img>').attr('src', e.target.result);
				var image = $('<img>').attr({
					src:e.target.result,
					class:"img-thumbnail",
					width:"140px",
					height:"140px"
				});
				$(image).appendTo('#image-thumbnails-final');

				// if there are more files run the file reader again
				if (k < numFilesFinal) {
					k++;
					readFileFinal();
				}
				
				console.log("done onload readerMiddle");
			};

			function readImageURLBeginning(input) {
					console.log("processing thumbnails beginning");
					imageFilesBeginning = document.getElementById('uploaded_beginning_images').files;
					// get the number of files
					numFilesBeginning = imageFilesBeginning.length;
					i=0;
					readFileBeginning();
				}

			function readImageURLMiddle(input) {
					console.log("processing thumbnails middle");
					imageFilesMiddle = document.getElementById('uploaded_middle_images').files;
					// get the number of files
					numFilesMiddle = imageFilesMiddle.length;
					console.log(numFilesMiddle);
					j=0;
					readFileMiddle();
					console.log("done reading middle image files");
				}

			function readImageURLFinal(input) {
					console.log("processing thumbnails final");
					imageFilesFinal = document.getElementById('uploaded_final_images').files;
					// get the number of files
					numFilesFinal = imageFilesFinal.length;
					k=0;
					readFileFinal();
				}
/***********************Image Browser *****************************/


// CHECK IF HAS HASH, if it has then automatically call the ajax page
if(window.location.href.indexOf("#") > 0) {
	split = window.location.href.split("#");
  	if(split[1]!=''){
   		call_ajax_page(split[1]);
   	}
}

// MENU JS
$(".slide-to").on('click',function(){
	console.log($(this).attr('href').substring(1));
	$('html,body').finish().animate({
	    scrollTop: $("#"+$(this).attr('href').substring(1)).offset().top
	}, 500, 'linear');
	return false;
});


// RESPONSIVE MENU
$("#menu-navigator").on('click',function(){
	parent = $(this).parent();
	if(parent.hasClass('active'))
		parent.removeClass('active');
	else
		parent.addClass('active');
	return false;
});


$(".btn-continue").on('click', function(){
	$(".step").fadeOut(300);
	$(this).parent().parent().next().fadeIn();
	return false;
});

$(".btn-back").on('click', function(){
	
	$(".step").fadeOut(300);
	$(this).parent().parent().prev().fadeIn();
	return false;
});


// SUBMITING FORM ON "CONTINUE" button
$(document).ready(function() {
	$(".btn-submit").on('click',function(){
		form = $(this).data('form-id');
		console.log(form);
		$("#"+form).submit();
		return false;
	});

});
