$(document).ready (function(){
	
	/* ============== check if login button/FB is clicked ======*/

		$('#login_fb').click(function(event) {
		    console.log("login fb clcik!");
		    if (getUrlVars()["page"] == "home" || getUrlVars()["page"] == false) {
		    	 console.log("redirectingh to facebook!");
		    	// $( ".redirect-message" ).append( "<p>Test</p>" );
		    	$("#login_fb").popover('show');
		    };
		});


	/* ============== check if login button/Google is clicked ======*/

		$('#login_google').click(function(event) {
		    console.log("login fb clcik!");
		    if (getUrlVars()["page"] == "home" || getUrlVars()["page"] == false) {
		    	 console.log("redirectingh to facebook!");
		    	// $( ".redirect-message" ).append( "<p>Test</p>" );
		    	$("#login_google").popover('show');
		    };
		});	
	/* ================= User info page / edit ===========*/
		$('.a-social-edit').on('click', function() {
		    var $row = jQuery(this).closest('tr');
		    var $columns = $row.find('td');

		    $columns.addClass('row-highlight');
		    var values = "";
		    
		    jQuery.each($columns, function(i, item) {
		        values = values +  item.innerHTML + ',';
		    });
		    var data = values.split(",");
	    	$("#social_media").each(function(){
		   		$(this).find("#id").val( data[0]);
				$(this).find("#username").val( data[1]);	
				$(this).find("#password").val( data[2]);
				$(this).find("#url").val( data[3]);								
			}); 
		});
/* ================= User info page / delete ===========*/
		$("#alert-sure-delete").hide();
		$('.a-social-delete').on('click', function() {
			$("#alert-sure-delete").show();
		    var $row = jQuery(this).closest('tr');
		    var $columns = $row.find('td');

		    $columns.addClass('row-highlight');
		    var values = "";
		    
		    jQuery.each($columns, function(i, item) {
		        values = values +  item.innerHTML + ',';
		    });
		    var data = values.split(",");
		    $("#alert-sure-delete .yes").on('click',function(){
				var id = data[0];
				window.location = '?page=user_info&status=delete_social_account&id='+id;
			});
	    	
		});
			
		/* ============== Confirm Password ===========*/
			$("#password").focusout(function( event ) {
				var password = $("#password").val();
				console.log(password);
				if(password.length < 6){
					$("#error-pass-length").removeClass("hidden");
					$( "#confirm-password" ).attr('disabled','disabled');
				}else{
					$("#error-pass-length").addClass("hidden"); 
					$( "#confirm-password" ).removeAttr('disabled');
					pass = $("#password").val();
				}
			});
			$( "#confirm-password" ).focusout(function( event ) {
				var password = $("#password").val();
				var confirm_pass = $("#confirm-password").val();
				if(password != confirm_pass){
					console.log("not match");
					$("#error-pass").removeClass("hidden"); 	
					$("#success-password").addClass("hidden"); 
					$("#a-next-to-social").attr('disabled','disabled');
				}else{
					$("#error-pass").addClass("hidden"); 	
					//$("#success-password").removeClass("hidden"); 
					$("#a-next-to-social").removeAttr('disabled');
				}

			});

		/* ============== invalid login ==============*/
		if(getUrlVars()["err"]){
			$('#user_login').modal('show');
		}
		/* ============= forgot password =============*/
		$("#forgot-password").on('click', function(e){
			    e.preventDefault();
			    $("#modal-login").slideUp();
			    $("div #modal-forgot-pass").removeClass("hidden");
		});
		$("#close-modal").on('click', function(e){
			    e.preventDefault();
			    $("#modal-login").slideDown();
			    $("div #modal-forgot-pass").addClass("hidden");
		});

		/* ================== user signup modal ======================================*/
		function open_personal_info(){
			$("#social-info").addClass('hidden');
			$("#browse-avatar").addClass('hidden');
			$("#personal-info").removeClass('hidden');
			$("#a-personal-info").addClass('selected');
			//$("#a-social-info").removeClass('selected');
			$("#a-browse-avatar").removeClass('selected');
		}
		$(function(){
			$("#a-personal-info").on('click', function(e){
			    e.preventDefault();
			   	open_personal_info();
			});
			$("#a-browse-avatar").on('click', function(e){
			    e.preventDefault();
			   	open_avatar();
			});
			$("#a-next-to-social").on('click', function(e){
			    e.preventDefault();
			   	open_avatar();
			});
			$('#a-next-to-social').keydown(function(e){
			    if (e.keyCode == 13) {
			        e.preventDefault();
			   		open_avatar();
			    }
			});

		});
		/* ============== New user  ===*/
		if(getUrlVars()["user_status"]){
			if (getUrlVars()["user_status"] == "new"){
				$().toastmessage('showToast', {
				    text     : 'You succefully login...',
				    sticky   : false,
				    type     : 'success',
				    position : 'top-center'
				  	//close    : function () {$('#social_media').modal('show');}
				});
			}else{

			}
		}
		/* ============== signup proccess==============*/
		if(getUrlVars()["signup_status"]){
			var signup = getUrlVars()["signup_status"];
			if ( signup== "ERROR" ) {
				$('#signup').modal('show');
			}else if(signup == "verify_email"){
					/*$().toastmessage('showToast', {
				    text     : 'Email verification already sent...',
				    sticky   : false,
				    type     : 'success',
				    position : 'top-center'*/
				    //close    : function () {window.location = "http://francisalbores.com/testsite/VideoSpinner/?page=home&signup_status=resend_email";}
				//});
			}else{
				
			}
		} 
		

		if(getUrlVars()["resend_email"]){
			if (getUrlVars()["resend_email"] == "ok") {
				$().toastmessage('showToast', {
			    text     : 'Email verification already sent...',
			    sticky   : false,
			    type     : 'success',
			    position : 'top-center'
			    //close    : function () {window.location = "http://francisalbores.com/testsite/VideoSpinner/?page=home&signup_status=resend_email";}
				});
			}
		}
		
		/* ============== reset password ==============*/
		if(getUrlVars()["status"]){
			if (getUrlVars()["status"] == "reset" ) {
				$('#reset-password').modal('show');
			}
		} 
		if(getUrlVars()["email"]){
			if (getUrlVars()["email"] == "ok" ) {
				$("#user_login").modal("show");
				 $("div #modal-login").addClass("hidden");
				 $("#modal-login").slideUp();
			     $("div #modal-forgot-pass").removeClass("hidden");
			}else{
				$("#user_login").modal("show");
				 $("div #modal-login").addClass("hidden");
				 $("#modal-login").slideUp();
			     $("div #modal-forgot-pass").removeClass("hidden");

			}
		}

		 $(".btnEdit").bind("click", Edit);
	    $(".btnDelete").bind("click", Delete);
	    $("#btnAdd").bind("click", Add);

    	$("#add_social").click(function (e){
    		console.log("add");
    		 Add();
    	});
});

		$(function(){
			$("#upload_link").on('click', function(e){
				console.log("upload");
			    e.preventDefault();
			    $("#avatar_prof:hidden").trigger('click');
			});
			$("#profile-avatar").on('click', function(e){
				console.log("upload");
			    e.preventDefault();
			    $("#avatar_prof:hidden").trigger('click');
			});

			$("#browse-beginning-images").on('click', function(e){
				console.log("upload beginning images");
			    e.preventDefault();
			    $("#uploaded_beginning_images:hidden").trigger('click');
			});

			$("#browse-middle-images").on('click', function(e){
				console.log("upload middle images");
			    e.preventDefault();
			    $("#uploaded_middle_images:hidden").trigger('click');
			});

			$("#browse-final-images").on('click', function(e){
				console.log("upload final images");
			    e.preventDefault();
			    $("#uploaded_final_images:hidden").trigger('click');
			});			

			$("input[name=uploaded_beginning_images").change(function() {
			    var names = [];
			    for (var i = 0; i < $(this).get(0).files.length; ++i) {
			        names.push($(this).get(0).files[i].name);
			    }
			    $("input[name=file]").val(names);
			    console.log("done browsing images");
			    //readImageURL($("input[name=file]").val);
			    console.log("done loading thumbnails");
			});

		});
function getUrlVars() {
    var vars = {};
    var parts =window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[ decodeURIComponent(key)] = decodeURIComponent(value);
    });
    return vars;
}

/* =========== function for new user ============= */


function Add(){
    $("#tbl_social TBODY").append(
        "<tr>"+
        "<td><input type='text' class='form-control small' id='username' name='username[]' placeholder='Username' maxlength='25' value='' required/></td>"+
        "<td><input type='text' class='form-control small' id='password' name='password[]' placeholder='Password' maxlength='25' value='' required/></td>"+
         "<td><input type='url' class='form-control large' id='url' name='url[]' placeholder='Account url' maxlength='100' value='' required/></td>"+
        //"<td><img src='images/disk.png' class='btnSave'><img src='images/delete.png' class='btnDelete'/></td>"+
        "<td><span class='glyphicon glyphicon-remove btnDelete'></span></td>"+
        "</tr>");
     
        $(".btnSave").bind("click", Save);     
        $(".btnDelete").bind("click", Delete);
};


function Save(){
    var par = $(this).parent().parent(); //tr
    var tdName = par.children("td:nth-child(1)");
    var tdPhone = par.children("td:nth-child(2)");
    var tdEmail = par.children("td:nth-child(3)");
    var tdButtons = par.children("td:nth-child(4)");
 
    tdName.html(tdName.children("input[type=text]").val());
    tdPhone.html(tdPhone.children("input[type=text]").val());
    tdEmail.html(tdEmail.children("input[type=text]").val());
    tdButtons.html("<href='#' class='btnEdit'>edit</a>");
    //tdButtons.html("<img src='images/delete.png' class='btnDelete'/><img src='images/pencil.png' class='btnEdit'/>");
 
    $(".btnEdit").bind("click", Edit);
    $(".btnDelete").bind("click", Delete);
};


function Edit(){
    var par = $(this).parent().parent(); //tr
    var tdName = par.children("td:nth-child(1)");
    var tdPhone = par.children("td:nth-child(2)");
    var tdEmail = par.children("td:nth-child(3)");
    var tdButtons = par.children("td:nth-child(4)");
 
    tdName.html("<input type='text' id='txtName' value='"+tdName.html()+"'/>");
    tdPhone.html("<input type='text' id='txtPhone' value='"+tdPhone.html()+"'/>");
    tdEmail.html("<input type='text' id='txtEmail' value='"+tdEmail.html()+"'/>");
   // tdButtons.html("<img src='images/disk.png' class='btnSave'/>");
   tdButtons.html("<href='#' class='btnSave'>Add</a>");
 
    $(".btnSave").bind("click", Save);
    $(".btnEdit").bind("click", Edit);
    $(".btnDelete").bind("click", Delete);
};


function Delete(){
    var par = $(this).parent().parent(); //tr
    par.remove();
};

function edit_button($,_this){
	$(_this).data('original-title','Edit Records').on('click', "a.edit",function(){
		_this = $(this).parent().parent().parent();
		$("table tr").removeClass('focus');
		_this.addClass('focus');
		_this = $(this).parent().parent().parent();
		$("#social_media").each(function(){
			$(this).find('form').prepend('<input type="hidden" name="id" value="'+$(_this).find('.id').data('id')+'" />')
			$(this).find("#username").val( $(_this).find('.username').html() );										
			$(this).find("#password").val( $(_this).find('.password').html() );
			$(this).find("#url").val( $(_this).find('.url').html() );
		}); 
	});
}