<?php
 error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
?>
<?php
	// INCLUDE MODEL File 
	include("models/cls_user.php");
	include("models/cls_openID.php");
	// CREATE OBJECT
	$user = new User();
	$user->login(); 
	$user->add();
	$user->verify_user();
	$user_info = $user->select_user();

	/* === openID Login === */
	$openID = New OpenID("sdsd");
	$loginUrl =   $openID->user_login_fb();
	$openID->insert_user_info_fb();
	$openID->user_login_google();
	$openID->insert_user_info_google();
	$info = $openID->get_user_info_fb();
?>

