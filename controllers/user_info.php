<?php
 error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
?>
<?php
	// INCLUDE MODEL File 
	include("models/cls_user.php");
	include("models/cls_openID.php");
	// CREATE OBJECT
	$user = new User();
	$user_info = $user->select_user();
	$social_media = $user-> select_social_media();
	$user->update();
	$user->update_social_media();
	$user->delete_social_media();
?>