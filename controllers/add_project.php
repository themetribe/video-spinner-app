
<?php 

include("models/cls_music_library.php");
include("models/cls_image.php");
include("models/cls_project.php");


$project = new Project();
$new_project_id = $project->get_new_project_id();
//$projects_list = $project->get_data(array('id'=>$new_project_id));


$image = new Image();
$imageWhereParam = array('project_id' => $new_project_id,
						'type' => 'intro_image');

$images = $image->get_data($imageWhereParam);


$music_lib = new Music_Library();
$music = $music_lib->get_all();

function script() { ?>
	<script>
		$(document).ready(function(){
		    $('.step').eq(0).fadeIn();
		});
	</script>
	<?php 
}
Func::footer_hook('script');
?>