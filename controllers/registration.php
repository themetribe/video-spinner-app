<?php
  error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
?>
<?php
	// INCLUDE MODEL File 
	include("models/cls_user.php");

	// CREATE OBJECT
	$user = new User();
	$user->reset_password();
	$user->resend_email();
	$user->change_email();

	function verify_email(){
		?>
		<div id="verify_email">
			<div class="container">
				<!-- <div class="well"> -->
		    		<!-- <h4>Email Verification</h4> --> 
					<div class="email-verification">
						<h2>Welcome <?php echo $_SESSION['first_name']; ?>!</h2> <br><p>You need to verify your e-mail address to start using your account.</p>
				
						<?php if (isset($_GET['email'])): ?>
							<?php if ($_GET['email'] == 'verified'): ?>
								<div class='alert alert-danger'>
									<p>Email already verified.</p>
								</div>
							<?php endif ?>
						<?php endif ?>
						<p>A confirmation letter with instructions was sent to</p>
						<p> <strong><?php echo $_SESSION['email']; ?></strong></p>
					</div>
					<form class="form-horizontal" role="form" method="post">
						<a href="?page=registration&signup_status=resend_email" name="btn-resend-email" id="btn-resend-email">Resend confirmation</a>
						<a href="?page=registration&signup_status=change_email" name="btn-change-email" id="btn-change-email" href="">Change e-mail</a>
					</form>	
				<!-- </div> -->
			</div>
		</div>	
		<?php
	}

	function change_email(){
		?>
		<div id="verify_email">
			<div class="container">
				<div class="email-verification">
		    		<form class="form-horizontal" role="form" method="post">
						<div class="clear_both"></div>
						<?php if (isset($_GET['signup_status']) && isset($_GET['s'])): ?>
						<?php if ($_GET['s'] == "error"): ?>
							<div class='alert alert-danger'>
								<p>Email already in use.</p>
							</div>
						<?php else: ?>
						 	<div class='alert alert-success'>
								<p>Your new e-mail address: <strong><?php echo $_SESSION['email']; ?></strong> </p><br>
								<p> You need to verify your e-mail address to start using your account.</p>
							</div>
							<div class="pull-left">
								<a href="?page=registration&signup_status=resend_email" name="btn-resend-email" id="btn-resend-email">Resend confirmation</a>
								<a href="?page=registration&signup_status=change_email" name="btn-change-email" id="btn-change-email" href="">Change e-mail</a>
							</div>
						<?php endif ?>
							
						<?php endif ?>
						<?php if (isset($_GET['email'])): ?>
							
						<?php endif ?>
						<form class="form-horizontal" role="form" method="post" action="">
						<div class="well">
							<div class="pull-left"><p>Your current e-mail address.</p></div>
							<div class="clear_both"></div>
							<div class="form-div">
								<div class="form-group">
				    				<label class="sr-only" for="txt_user_email">Email address</label>
				    				<div class="col-sm-10">
				    					<input class="form-control email-change" type="email" name="user_email" id="txt_user_email" value="<?php echo isset($_SESSION['email'])? $_SESSION['email']:""; ?>"/>
				    				</div>
									<input type="submit" value="Save" name="btn-change-email" id="btn-change-email" class="btn btn-primary btn-lg" />
								</div>
							</div>
						</div>	
					</form> 
				</div>
			</div>
		</div>		
		<?php
	}
?>

