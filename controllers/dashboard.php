<?php

	// INCLUDE MODEL File 
	include('models/cls_project.php');
	include('models/cls_openID.php');
	include("models/cls_user.php");
	// CREATE OBJECT
	$projects = new Project();
	$whereParam = array('email' => $_SESSION["email"] );
	$project_list = $projects->get_data($whereParam);

	$openID = New OpenID();
	$data = $openID->add_social_acount();
	$openID->logout();

	$user = New User();
	$user->update_avatar();
?>


