<?php




function save_project(){ 
	$id = 1; //must be GET
	// id of project created
	$project = new Project();	
	$project->save_details($id);
	$project->save_music($id, $_POST['music_included']);	
}
function script(){
	?>
	<script>
		jQuery(".tagManager").tagsManager();
		
		$("#description").tagsManager({
			deleteTagsOnBackspace: false,
			//prefilled: ["Pisa", "Rome"],
			CapitalizeFirstLetter: true,
				//preventSubmitOnEnter: true,
			typeahead: true,
			typeaheadAjaxSource: null,
			typeaheadSource: ["Pisa", "Rome", "Milan", "Florence", "New York", "Paris", "Berlin", "London", "Madrid"],
			delimeters: [44, 188, 13],
			backspace: [8],
			blinkBGColor_1: '#FFFF9C',
			blinkBGColor_2: '#CDE69C',
			hiddenTagListName: 'hiddenTagListA',
			typeaheadAjaxPolling: false
		});

	</script>
	<?php
}
Func::footer_hook('script');